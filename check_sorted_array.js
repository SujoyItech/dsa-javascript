function checkSortedArray(arr,index=0){
    if(index == arr.length-1) return true;
    return arr[index] < arr[index+1] && checkSortedArray(arr,index+1);
}

console.log(checkSortedArray([1,2,4,3,5]))