function bubbleSort(arr,r=arr.length,c=0){
    if(r == 0) return arr;
    if(c<r){
        let temp;
        if(arr[c]>arr[c+1]){
            temp = arr[c];
            arr[c] = arr[c+1];
            arr[c+1] = temp;
        }
        return bubbleSort(arr,r,c+1);
    }else{
        return bubbleSort(arr,r-1,0);
    }
}

console.log(bubbleSort([5,4,2,3,1,7,9]));
