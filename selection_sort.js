function selectionSort(arr, r = arr.length, c = 0, max=0){
    if(r==0) return arr;
    if(c < r){
         if(arr[c] > arr[max]){
            return selectionSort(arr,r,c+1,c);
        }else{
            return selectionSort(arr,r,c+1,max);
        }
    }else{
        const temp = arr[max];
        arr[max] = arr[r-1];
        arr[r-1] = temp;
        return selectionSort(arr,r-1,0,0)
    }
}

console.log(selectionSort([4,3,2,1]));
